$(function() {
  function InfluxDB2ViewModel(parameters) {
    
    this.settings = parameters[0];

    this.hasTokenPlaceholder = ko.observable();

    let updateTokenPlaceholder = () => {
      this.hasTokenPlaceholder(this.settings.settings.plugins.influxdb2.has_token() ? '(Saved)' : '');
    };

    this.onBeforeBinding = () => {
      updateTokenPlaceholder();
      this.settings.settings.plugins.influxdb2.has_token.subscribe(updateTokenPlaceholder);
    }

    this.connectionTest = async () => {
      await this.settings.saveData(undefined);
      //await OctoPrint.settings.savePluginSettings('influxdb2', this.settings.settings);

      const apiTestResponse = await fetch('/api/plugin/influxdb2', {
        method: 'GET',
      });

      $(`#influx-connection-${apiTestResponse.ok ? 'success' : 'failure'}`).show();
      $(`#influx-connection-${!apiTestResponse.ok ? 'success' : 'failure'}`).hide();
    }
  }

  OCTOPRINT_VIEWMODELS.push([
    InfluxDB2ViewModel,
    ['settingsViewModel'],
    ['#settings_plugin_influxdb2']
  ])
})