import octoprint.printer
from influxdb_client import Point, WritePrecision

from frozendict import frozendict

class InfluxPrinterCallback(octoprint.printer.PrinterCallback):
  def __init__(self, _settings, influxWrite):
    self.influx = influxWrite
    self._settings = _settings
  
  def update_influx(self, influxWrite):
    self.influx = influxWrite

  def disable(self):
    self.influx = None
  
  def on_printer_add_temperature(self, data):
    if self.influx == None:
      return

    point = Point(self._settings.get(['measurement_temps']))

    machine = self._settings.get(['opt_machine'])
    if (machine or '') != '':
      point.tag('machine', machine)
    
    point.time(data['time'], WritePrecision.S)
    process_dict(point, '', data)
    
    self.influx.write(bucket = self._settings.get(['bucket']),
                      record = point)
  
  def on_printer_send_current_data(self, data):
    if self.influx == None:
      return

    point = Point(self._settings.get(['measurement_status']))

    machine = self._settings.get(['opt_machine'])
    if (machine or '') != '':
      point.tag('machine', machine)
    
    process_dict(point, '', data)
    
    self.influx.write(bucket = self._settings.get(['bucket']),
                      record = point)

def process_dict(point, prefix, data):
  if prefix != '': prefix += '_'

  for key in data:
    fullkey = prefix + key
    if isinstance(data[key], frozendict) or isinstance(data[key], dict):
      process_dict(point, fullkey, data[key])
    elif prefix != '':
      point.field(fullkey, data[key])