import octoprint.plugin
import requests


from influxdb_client import InfluxDBClient, WriteOptions
from influxdb_client.client.write_api import SYNCHRONOUS

from .InfluxPrinterCallback import InfluxPrinterCallback
from .SoftwareUpdate import SoftwareUpdate

class InfluxDB2Plugin(octoprint.plugin.AssetPlugin,
                      octoprint.plugin.ShutdownPlugin,
                      octoprint.plugin.SimpleApiPlugin,
                      octoprint.plugin.SettingsPlugin,
                      octoprint.plugin.StartupPlugin,
                      octoprint.plugin.TemplatePlugin):

  ##############################
  # STARTUP PLUGIN DEFINITIONS #
  ##############################
  def on_after_startup(self):
    self.influx = None
    self.influxLogger = InfluxPrinterCallback(self._settings, None)
    self._printer.register_callback(self.influxLogger)
    self.reconnectInflux()

  ###############################
  # SHUTDOWN PLUGIN DEFINITIONS #
  ###############################
  def on_shutdown():
    self.closeInflux()

  ###############################
  # SETTINGS PLUGIN DEFINITIONS #
  ###############################

  def get_settings_defaults(self):
    return dict(
      host=None,
      token=None,
      has_token=False,
      org=None,
      bucket=None,
      measurement_status=None,
      measurement_temps=None,
      opt_machine=None
    )
  
  def get_settings_restricted_paths(self):
    return {
      'admin': [['host'], ['org'], ['bucket'], ['has_token']],
      'never': [['token']]
    }

  def on_settings_save(self, data):
    if ('token' in data):
      data['has_token'] = (data['token'] or '') != ''

    octoprint.plugin.SettingsPlugin.on_settings_save(self, data)
    self.reconnectInflux()

  ###############################
  # TEMPLATE PLUGIN DEFINITIONS #
  #       (Settings Page)       #
  ###############################

  def get_template_configs(self):
    return [
      dict(type='settings', custom_bindings=True),
    ]

  def get_assets(self):
    return dict(
      js=['js/influxdb2.js']
    )

  #####################
  # SIMPLE API PLUGIN #
  #####################
  def is_api_adminonly(self):
    return True

  def on_api_get(self, request):
    self.influxApiTest()

  ############################
  # INFLUX RELATED FUNCTIONS #
  ############################

  def closeInflux(self):
    self.influxLogger.disable()
    if self.influx != None:
      self.influx.close()
      self.influx = None

  def reconnectInflux(self):
    self.closeInflux()

    try:
      self.influxApiTest()

      self.influx = InfluxDBClient(url = self._settings.get(['host']),
                                   token = self._settings.get(['token']),
                                   org = self._settings.get(['org']))
      self.influxLogger.update_influx(self.influx.write_api())

      self._logger.info('Connected to InfluxDB -- Logging Enabled')
    except Exception as e:
      self._logger.warning('Could not connect to InfluxDB -- Logging Disabled')

  def influxApiTest(self):
    for key in self.get_settings_defaults():
      if key.startswith('opt_'): continue

      val = self._settings.get([key])
      if (val or '') == '':
        self._logger.error('Setting not defined: %s' % key)
        raise RuntimeException('Not all settings are filled out!')

    # Test API and issue warning
    try:
      r = requests.post('%s/api/v2/write' % self._settings.get(['host']),
                       params = {
                        'org': self._settings.get(['org']), 
                        'bucket': self._settings.get(['bucket']), 
                       },
                       headers = {'Authorization': 'Token %s' % self._settings.get(['token'])})
      r.raise_for_status()
    except requests.exceptions.RequestException as e:
      self._logger.error('Could not ping Influx Host with given credentials!')
      raise e
    else:
      self._logger.info('Successfully verified credentials')

  ###############
  # UPDATE HOOK #
  ###############
  def get_update_information(self):
    return dict(
      influxdb2 = dict(
        displayName = self._plugin_name,
        displayVersion = self._plugin_version,

        # https://docs.octoprint.org/en/master/bundledplugins/softwareupdate.html#version-checks
        type = 'python_checker',
        python_checker = SoftwareUpdate(self._plugin_version),

        # Update URL via pip
        pip = SoftwareUpdate.pip_latest_url
      )
    )

__plugin_implementation__ = InfluxDB2Plugin()
__plugin_pythoncompat__ = '>3,<4'

__plugin_hooks__ = {
  "octoprint.plugin.softwareupdate.check_config": __plugin_implementation__.get_update_information
}