import requests

class SoftwareUpdate:
  def __init__(self, current_version):
    self.current_version = current_version

  def get_latest(self, target, check, *args, **kwargs):

    # Locally installed version
    current_version = self.current_version
    information = {
      'local': {
        'name': 'v%s' % current_version,
        'value': 'v%s' % current_version,
      },
      'remote': {
        'name': 'offline',
        'value': 'offline',
      }
    }

    # Fetch remote version
    try:
      gitlab_latest_release_url = "https://gitlab.com/api/v4/projects/%s/releases" % 43844128
      releases_req = requests.get(gitlab_latest_release_url)
      release_data = releases_req.json()
      latest_v = release_data[0]['tag_name']
      information['remote'] = {
        'name': latest_v,
        'value': latest_v,
      }
    except requests.exceptions.RequestException:
      pass

    is_latest = information['local']['name'] == information['remote']['name']

    # Done!
    return information, is_latest

  pip_latest_url = "https://gitlab.com/moltenbytes32/octoprint-influxdb2/-/archive/{target}/octoprint-influxdb2-{target}.zip"