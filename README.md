# OctoPrint-InfluxDB2

Pushes metrics to InfluxDB v2.0+.

## Setup
Currently installation is only supported via copying the files under `octoprint_influxdb2` to `~/.octoprint/plugins/`.

One day (hopefully soon) this is installable via pip and the official plugin repository.

## Configuration

Configuration of the plugin is relatively straightforward, and takes place entirely through the settings page in OctoPrint:

Setting            | Description
------------------ | -----------
Influx DB URL      | The URL to your InfluxDB Host
Access Token       | The API token you want this service to use.
Organization       | The organization in InfluxDB to push metrics to
Bucket             | The bucket in InfluxDB to push metrics to
Status Measurement | The measurement name to push status readouts to
Temps Measurement  | The measurement name to push temperature readouts to
Machine Tag        | An optional tag to apply to all data

For security reasons, all settings are admin-only. The Access token is additionally unavailable after it is input, and isn't available on REST APIs. It only needs write access to a single bucket.

## Supported Metrics
Metrics are obtained by dynamically decomposing the dictionaries present on the [status event](https://docs.octoprint.org/en/master/modules/printer.html#octoprint.printer.PrinterCallback.on_printer_send_current_data) and [temperature event](https://docs.octoprint.org/en/master/modules/printer.html#octoprint.printer.PrinterCallback.on_printer_add_temperature). The payloads are recursively processed, and the keys are joined with '_'. Each are tagged with their configured measurement field.

For example, the current job's file size is at `job_file_size` on the status measurement. Tool1's actual temperature is at `tool1_actual` on the temperature measurement. 

## Development

Development is pretty straightforward thanks to docker -- create the `octoprint_data` folder, then `docker compose up`, and connect to http://localhost:8080.

I like to `while true; do docker compose up; done` so that I can restart the server with `^c`. If you do this, simply `^z` then `kill %1` to exit.